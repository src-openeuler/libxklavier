Name:           libxklavier
Version:        5.4
Release:        22
Summary:        Library with X keyboard related functions
License:        LGPLv2+
URL:            https://www.freedesktop.org/wiki/Software/LibXklavier
Source0:        http://people.freedesktop.org/~svu/%{name}-%{version}.tar.bz2

Patch9001:      autogen.sh-Drop-intltool-requirement.patch

BuildRequires:  make libX11-devel libxml2-devel iso-codes-devel glib2-devel libxkbfile-devel
BuildRequires:  libXi-devel gobject-introspection-devel
Requires:       iso-codes

%description
This package is a library that provides a high-level API for X keyboard extensions, called XKB.
This library can support XFree86 and other commercial X servers.

%package  devel
Summary:    Libxklavier development files
Requires:   %{name} = %{version}-%{release} libxml2-devel

%description  devel
This package contains all necessary include files and libraries needed to develop
applications that require these.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --with-xkb-base='%{_datadir}/X11/xkb' --with-xkb-bin-base='%{_bindir}' --disable-vala
%make_build

%check
make check

%install
%make_install
%delete_la_and_a

%post
/sbin/ldconfig
%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING.LIB
%{_libdir}/{girepository-1.0/Xkl-1.0.typelib,libxklavier.so.*}

%files devel
%defattr(-,root,root)
%{_includedir}/libxklavier/*.h
%{_libdir}/{pkgconfig/libxklavier.pc,libxklavier.so}
%{_datadir}/gir-1.0/Xkl-1.0.gir

%files help
%defattr(-,root,root)
%doc NEWS README
%{_datadir}/gtk-doc/*

%changelog
* Tue Feb 28 2023 zhouwenpei <zhouwenpei@h-partners.com> - 5.4-22
- disable vala to fix build error

* Tue Sep 6 2022 kouwenqi <kouwenqi@kylinos.cn> - 5.4-21
- Drop intltool requirement from autogen.sh

* Wed Apr 6 2022 liuyumeng1 <liuyumeng5@h-partners.com> -5.4-20
- enable tests

* Wed Jul 21 2021 yushaogui <yushaogui@huawei.com> - 5.4-19
- delete a buildrequires for gdb

* Sat Mar 21 2020 yanglijin <yanglijin@huawei.com> - 5.4-18
- modify buildrequires

* Wed Dec 11 2019 duyeyu <duyeyu@huawei.com> - 5.4-17
- Package init
